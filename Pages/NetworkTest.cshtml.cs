﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Net;
using System.Diagnostics;
using System.Web;

namespace DotnetRazorSampleWebApp.Pages
{
    public class NetworkTestModel : PageModel
    {
        public string Message { get; set; }

        private WebClient client = new WebClient();

        public List < string > Results = new List< string > ();

        public List <string> TestURLs = new List<string> ()
        {
            "http://google.com",
            "http://nubeva.com",
            "http://smh.com.au"
        };

        public void OnGet()
        {
            string res;
            Message = "This page tests if this ASP.NET Core app can connect to a number of different web servers.";
            foreach (var uri in TestURLs) {
                res = connect(uri);
                Results.Add(res);
            }
        }

        public string connect(string RequestUri)
        {
            try {
                var response = client.DownloadString(RequestUri);
                return "<p><span style=\"color:darkgreen\">Successfully</span> requested " + RequestUri + "</p><p>Response is:</p> <textarea>" + response + "</textarea>";
            }
            catch (Exception e)
            {
                return "<span style=\"color:darkred\">Failed</span> to request " + RequestUri;
            }
        }
    }
}
